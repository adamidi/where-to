import { AppPage } from './app.po';

describe('where-to App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display an app title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('WhereToApp');
  });
});
