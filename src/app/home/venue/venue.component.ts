import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wt-venue',
  templateUrl: './venue.component.html'
})
export class VenueComponent implements OnInit {
  @Input() venue: any;
  category: string;

  constructor() {}

  ngOnInit() {
    this.category = this.venue.categories.map(category => category.name).join(', ');
  }
}
