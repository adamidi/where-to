import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'wt-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  busy: boolean = false;
  error: boolean = false;
  recommendations: any[] = [];

  constructor(
    private appService: AppService
  ) {}

  ngOnInit() {}

  onSubmitQuery(requestObject) {
    this.busy = true;
    this.error = false;

    this.appService.getRecommendations(requestObject)
      .subscribe(
        (response) => this.getRecommendationsSuccess(response),
        (error) => this.getRecommendationsError(error)
      );
  }

  private getRecommendationsSuccess(response) {
    this.busy = false;
    this.recommendations = response.response.groups[0].items;
  }

  private getRecommendationsError(error) {
    this.busy = false;
    this.error = true;
  }
}
