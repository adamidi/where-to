import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule  } from '@angular/router/testing';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('raises the submitQuery event on submit with correct value', () => {
    component.section = 'section';
    component.location = 'location';
    component.openNow = true;
    component.sortByDistance = false;

    const requestObject = {
      section: 'section',
      near: 'location',
      openNow: true,
      sortByDistance: false
    }

    component.submitQuery.subscribe(value => expect(value).toEqual(requestObject));
    component.onSubmit();
  });

});
