import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'wt-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() submitQuery = new EventEmitter<any>();
  location: string = 'Amsterdam';
  openNow: boolean = false;
  section: string = 'topPicks';
  sections: string[];
  sortByDistance: boolean = true;

  constructor() {}

  ngOnInit() {
    this.sections = [
      'food',
      'drinks',
      'coffee',
      'shops',
      'arts',
      'outdoors',
      'sights',
      'trending',
      'topPicks'
    ];

    this.onSubmit();
  }

  onSubmit() {
    let requestObject = {
      section: this.section,
      near: this.location,
      openNow: this.openNow,
      sortByDistance: this.sortByDistance
    }

    this.submitQuery.emit(requestObject);
  }
}
