import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule  } from '@angular/router/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { HomeComponent } from './home.component';
import { SearchComponent } from './search/search.component';
import { VenueComponent } from './venue/venue.component';
import { AppService } from '../app.service';

class MockAppService {
  getRecommendations(): Observable<any> {
        return Observable.of({});
    }
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let appService: AppService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, SearchComponent, VenueComponent ],
      imports: [ FormsModule ],
      providers: [ { provide: AppService, useClass: MockAppService }, HttpClient, HttpHandler ]
    })
    .compileComponents();
    appService = TestBed.get(AppService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should correctly handle successful fetch of venue recommendations', () => {
    const mockResponse = {
      response: {
        groups: [{
          items: 'recommendations'
        }]
      }
    };
    spyOn(appService, 'getRecommendations').and.returnValue(
        Observable.of(mockResponse)
    );
    component.onSubmitQuery({});
    expect(appService.getRecommendations).toHaveBeenCalled();
    expect(component.recommendations).toEqual('recommendations');
  });

  it('should correctly handle failed fetch of venue recommendations', () => {
    spyOn(appService, 'getRecommendations').and.returnValue(
        Observable.throw(new Error('error'))
    );
    component.onSubmitQuery({});
    expect(appService.getRecommendations).toHaveBeenCalled();
    expect(component.error).toBeTruthy();
  });
});
