import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { AppConfig } from './app.config';

@Injectable()
export class AppService {
  private apiVersion = '20181118';
  private apiUrl = 'https://api.foursquare.com/v2/venues/explore';

  constructor(
    private http: HttpClient
  ) {}

  getRecommendations(requestObject: any): Observable<any> {
    const params = {
      client_id: AppConfig.client_id,
      client_secret: AppConfig.client_secret,
      v: this.apiVersion,
      ...requestObject
    };
    return this.http.get(this.apiUrl, { params });
  }
}
